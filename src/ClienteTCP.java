import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClienteTCP {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            // Se establece la conexión con el servidor en localhost y puerto 50005
            System.out.println("Conectando al servidor...");
            Socket cliente = new Socket("localhost", 50005);
            System.out.println("Conexión establecida.");

            // Streams de entrada y salida para comunicarse con el servidor
            DataInputStream entrada = new DataInputStream(cliente.getInputStream());
            DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());

            // Comunicación con el servidor
            String mensaje;
            while (true) {
                // Se lee el mensaje del servidor
                mensaje = entrada.readUTF();
                System.out.println("Servidor: " + mensaje);

                if (mensaje.equalsIgnoreCase("salir")) {
                    // Si el servidor envía "salir", se termina la comunicación y se cierra la conexión
                    System.out.println("El servidor ha cerrado la conexión.");
                    break;
                }

                // Se solicita la entrada del usuario y se envía al servidor
                String respuesta = scanner.nextLine();
                salida.writeUTF(respuesta);
                if (respuesta.equalsIgnoreCase("salir")) {
                    // Si el usuario envía "salir", se solicita el cierre de la conexión y se termina la comunicación
                    System.out.println("Solicitando cierre de la conexión...");
                    break;
                }
            }

            // Cierre de conexiones
            salida.close();
            entrada.close();
            cliente.close();
            System.out.println("Conexión cerrada.");

        } catch (IOException e) {
            System.err.println("Error al conectar con el servidor: " + e.getMessage());
        }
    }
}
