import java.io.IOException;
import java.net.*;

public class ClienteUDP {
    public static void main(String[] args) {

        try {
            // Crear un socket UDP
            DatagramSocket datagramSocket = new DatagramSocket();
            InetAddress serverAddr = InetAddress.getByName("localhost");

            // Operaciones a realizar
            String[] operaciones = {
                    "5,5,+", // Suma
                    "10,2,-", // Resta
                    "3,4,*", // Multiplicación
                    "6,2,/", // División
                    "salir" // Salir
            };

            for (String operacion : operaciones) {
                // Construir el paquete
                byte[] mensajeBytes = operacion.getBytes();
                DatagramPacket paquete = new DatagramPacket(mensajeBytes, mensajeBytes.length,
                        serverAddr, 50005);

                // Enviar el paquete
                datagramSocket.send(paquete);

                // Si la operación es "salir", terminamos
                if (operacion.equalsIgnoreCase("salir")) {
                    System.out.println("Desconectado del servidor.");
                    break;
                }

                // Recibir la respuesta del servidor
                byte[] respuestaBytes = new byte[1024];
                DatagramPacket respuestaPaquete = new DatagramPacket(respuestaBytes, respuestaBytes.length);
                datagramSocket.receive(respuestaPaquete);
                String respuesta = new String(respuestaPaquete.getData(), 0, respuestaPaquete.getLength());
                System.out.println("Resultado de la operación: " + respuesta);
            }

            datagramSocket.close();

        } catch (IOException e) {
            System.out.println("Error en el cliente: " + e.getMessage());
        }

    }
}
