import java.io.IOException;
import java.net.*;

public class ServidorUDP {
    public static void main(String[] args) {

        try {
            // Crear un socket UDP en el puerto 50005
            InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 50005);
            DatagramSocket socket = new DatagramSocket(addr);
            System.out.println("Servidor iniciado. Esperando conexiones...");

            while (true) {
                // Recibir paquete del cliente
                DatagramPacket paquete = new DatagramPacket(new byte[1024], 1024);
                socket.receive(paquete);

                // Convertir datos recibidos a String
                String mensaje = new String(paquete.getData(), 0, paquete.getLength());

                // Verificar si el cliente quiere salir
                if (mensaje.equalsIgnoreCase("salir")) {
                    System.out.println("Cliente ha cerrado la conexión.");
                    continue;
                }

                // Analizar la solicitud del cliente
                String[] partes = mensaje.split(",");
                if (partes.length != 3) {
                    System.out.println("Solicitud del cliente inválida.");
                    continue;
                }

                // Extraer operandos y operador
                double operando1 = Double.parseDouble(partes[0]);
                double operando2 = Double.parseDouble(partes[1]);
                char operador = partes[2].charAt(0);

                // Realizar la operación
                double resultado = calcular(operando1, operando2, operador);

                // Enviar el resultado al cliente
                byte[] respuesta = Double.toString(resultado).getBytes();
                DatagramPacket respuestaPaquete = new DatagramPacket(respuesta, respuesta.length,
                        paquete.getAddress(), paquete.getPort());
                socket.send(respuestaPaquete);
            }

        } catch (IOException e) {
            System.out.println("Error en el servidor: " + e.getMessage());
        }

    }

    // Método para realizar operaciones matemáticas
    private static double calcular(double op1, double op2, char operador) {
        switch (operador) {
            case '+':
                return op1 + op2;
            case '-':
                return op1 - op2;
            case '*':
                return op1 * op2;
            case '/':
                if (op2 != 0) {
                    return op1 / op2;
                } else {
                    return Double.NaN; // Indefinido
                }
            default:
                return Double.NaN; // Indefinido
        }
    }
}
