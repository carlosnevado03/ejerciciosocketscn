import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorTCP {
    public static void main(String[] args) {
        try {
            // Se inicia el servidor en el puerto 50005
            ServerSocket servidor = new ServerSocket(50005);
            System.out.println("Servidor iniciado. Esperando conexiones...");

            while (true) {
                // Espera la conexión de un cliente
                Socket cliente = servidor.accept();
                System.out.println("Cliente conectado.");

                // Streams de entrada y salida para comunicarse con el cliente
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());

                // Comunicación con el cliente
                while (true) {
                    // Se envía el primer mensaje al cliente solicitando el primer operando o "salir"
                    salida.writeUTF("Dime el primer operando o \"salir\": ");
                    String operando1 = entrada.readUTF();
                    if (operando1.equalsIgnoreCase("salir")) {
                        // Si el cliente envía "salir", se envía "salir" al cliente y se termina la comunicación
                        salida.writeUTF("salir");
                        break;
                    }

                    // Se solicita el segundo operando y el operador al cliente
                    salida.writeUTF("Dime el segundo operando: ");
                    String operando2 = entrada.readUTF();
                    salida.writeUTF("Dime el operador [+,-,*,/]: ");
                    String operador = entrada.readUTF();

                    // Se realiza el cálculo de acuerdo a la operación solicitada por el cliente
                    double resultado = calcular(Double.parseDouble(operando1), Double.parseDouble(operando2), operador.charAt(0));
                    // Se envía el resultado al cliente
                    salida.writeUTF(String.valueOf(resultado));
                }

                // Se cierra la conexión con el cliente
                System.out.println("Cliente desconectado.");
                cliente.close();
            }
        } catch (IOException e) {
            System.err.println("Error en el servidor: " + e.getMessage());
        }
    }

    // Método para realizar operaciones matemáticas
    private static double calcular(double op1, double op2, char operador) {
        switch (operador) {
            case '+':
                return op1 + op2;
            case '-':
                return op1 - op2;
            case '*':
                return op1 * op2;
            case '/':
                if (op2 != 0) {
                    return op1 / op2;
                } else {
                    // se ejecuta cuando el operador proporcionado por el cliente no es reconocido como una de las operaciones definidas (+, -, *, /).
                    return Double.NaN;
                }
            default:
                return Double.NaN;
        }
    }
}
